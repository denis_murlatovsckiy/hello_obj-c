//
//  Person.m
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "Person.h"

@implementation Person

- (NSString *) fullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

- (instancetype)init
{
    return [self initWithFirstName:@"noname"
                       andLastName:@"unknown"];
}

- (instancetype)initWithFirstName:(NSString *)fName
                      andLastName:(NSString *)lName
{
    self = [super init];
    if (self) {
        _firstName = fName;
        _lastName  = lName;
    }
    
    //для отладки
    NSLog(@"%@ with %@ created", [super description],[self fullName]);
    
    return self;
}

+ (instancetype) personWithFirstName:(NSString *)fName
                         andLastName:(NSString *)lName {
    return [[self alloc] initWithFirstName:fName
                               andLastName:lName];
}

- (NSString *)description
{
    return [self fullName];
}

- (void)dealloc
{
    NSLog(@"%@ with %@ deleted", [super description],[self fullName]);
    
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    
    [coder encodeObject:self.firstName forKey:@"PersonFirstName"];
    [coder encodeObject:self.lastName forKey:@"PersonLastName"];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    return [self initWithFirstName:[coder decodeObjectForKey:@"PersonFirstName"]
                       andLastName:[coder decodeObjectForKey:@"PersonLastName"]];
}

@end

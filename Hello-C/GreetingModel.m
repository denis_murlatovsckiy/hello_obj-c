//
//  GreetingModel.m
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "GreetingModel.h"
#import "Person.h"


@interface GreetingModel ()

@property NSArray *memberList;
@property (nonatomic) NSUInteger currentMember;

@end

@implementation GreetingModel

- (void) setCurrentMember:(NSUInteger)currentMember {
    if (currentMember >= self.memberList.count) return;
    _currentMember = currentMember;
}

- (Greeting *) currentData {
    return self.memberList[self.currentMember];
}

- (BOOL) madeGreetingShiftWithTag:(NSInteger)shiftTag {
    if (shiftTag < firstRecord || shiftTag > lastRecord) return NO;
    NSUInteger oldCurrent = self.currentMember;
    switch (shiftTag) {
        case firstRecord:   self.currentMember = 0; break;
        case priorRecord:   --self.currentMember;   break;
        case nextRecord:    ++self.currentMember;   break;
        case lastRecord:    self.currentMember = self.memberList.count-1; break;
    }
    return oldCurrent != self.currentMember;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
#if 9
        _memberList = @[ @"C", @"Smalltalk", @"Objective-C", @"Swift", /*];
        _memberList = @[*/ [Person personWithFirstName:@"Dennies"
                                         andLastName:@"Ritchie"],
                         [Person personWithFirstName:@"Alan"
                                         andLastName:@"Kay"],
                         [Person personWithFirstName:@"Brad"
                                         andLastName:@"Cox"],
                         [Person personWithFirstName:@"Chris"
                                         andLastName:@"Lattner"]
                         ];
        
//        [_memberList writeToFile:@"ProgLang.txt" atomically:YES];
//        [NSKeyedArchiver archiveRootObject:_memberList toFile:@"ProgLang.txt"];
        [NSKeyedArchiver archiveRootObject:_memberList toFile:@"Person.txt"];
#else
//        _memberList = [NSArray arrayWithContentsOfFile:@"ProgLang.txt"];
//        _memberList = [NSKeyedUnarchiver unarchiveObjectWithFile:@"ProgLang.txt"];
        _memberList = [NSKeyedUnarchiver unarchiveObjectWithFile:@"Person.txt"];
#endif
        _currentMember = 0;
        NSLog(@"%@ created", self);
        
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"%@ deleted", self);
}

@end

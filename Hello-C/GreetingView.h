//
//  GreetingView.h
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GreetingController.h"

@interface GreetingView : NSObject

@property (weak) GreetingController *controller;

- (BOOL)madeGreeting;
- (void) displayGreeting;

@end

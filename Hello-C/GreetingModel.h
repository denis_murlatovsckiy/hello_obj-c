//
//  GreetingModel.h
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Greeting.h"

typedef enum { firstRecord=1, priorRecord, nextRecord, lastRecord} ShiftType;

@interface GreetingModel : NSObject

- (BOOL) madeGreetingShiftWithTag:(NSInteger)shiftTag;
- (Greeting *) currentData;

@end

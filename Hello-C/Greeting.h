//
//  Greeting.h
//  Hello-C
//
//  Created by админ on 25.04.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Greeting : NSObject

@property id greetingData;

+ (instancetype) greetingWithString:(NSString *)greetingString;

@end

//
//  GreetingController.h
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Greeting.h"


typedef enum { nilGreeting, engGreeting, rusGreeting} GreetingType;

@interface GreetingController : NSObject

@property Greeting *greeting;

- (void) makeGreetingTypeWithTag:(NSInteger)greetingTag;
- (void) makeGreetingShiftWithTag:(NSInteger)shiftTag;

- (void) run;

@end

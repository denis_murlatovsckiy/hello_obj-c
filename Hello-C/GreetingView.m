//
//  GreetingView.m
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "GreetingView.h"

typedef enum { firstRecord=1, priorRecord, nextRecord, lastRecord} ShiftType;

@implementation GreetingView

- (void) displayGreeting
{
    NSLog(@"\n--- %@!", self.controller.greeting);
}



- (char)inputTag:(const char *)prompt {
    char ch;
    printf("%s", prompt);
    scanf(" %c", &ch);
    return ch;
}

- (BOOL)madeGreeting {
    char ch;
    NSInteger tag = 0;
    while (YES) {
        ch = [self inputTag:
              " 'e' - English\t"
              " 'r' - Russian\n"
              " 'f' - first\t"
              " 'p' - prior\n"
              " 'n' - next\t"
              " 'l' - last\n"
              " 'q' - quit\n"
              ];
        
        switch (ch) {
            case 'q': return NO;
                break;
            case 'e': tag = engGreeting;
                break;
            case 'r': tag = rusGreeting;
                break;
            default:
                switch (ch) {
                    case 'f': tag = firstRecord;
                        break;
                    case 'p': tag = priorRecord;
                        break;
                    case 'n': tag = nextRecord;
                        break;
                    case 'l': tag = lastRecord;
                        break;
                    default:  continue;
                }
                [self.controller makeGreetingShiftWithTag:tag];
                return YES;
        }
        [self.controller makeGreetingTypeWithTag:tag];
        return YES;
    }
}
@end

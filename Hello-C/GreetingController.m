//
//  GreetingController.m
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "GreetingController.h"
#import "Greeting.h"
#import "GreetingModel.h"
#import "GreetingView.h"
#import "Person.h"


@interface GreetingController ()

@property NSDictionary  *preface;
@property GreetingModel *model;
@property GreetingView  *view;
@property NSUInteger     greetingTag;

@end

@implementation GreetingController




- (void) makeGreeting {
    _greeting = [Greeting greetingWithString:[NSString stringWithFormat:@"%@, %@", self.preface[@(_greetingTag)], [self.model currentData]]];
}

- (void) makeGreetingTypeWithTag:(NSInteger)greetingTag {
    if (_greetingTag == greetingTag) return;
    if (greetingTag < 0 || greetingTag > self.preface.count) return;
    _greetingTag = greetingTag;
    [self makeGreeting];
}

- (void) makeGreetingShiftWithTag:(NSInteger)shiftTag {
    if ([self.model madeGreetingShiftWithTag:shiftTag])
        [self makeGreeting];
}


- (void) run {
    while ( [self.view madeGreeting] ) {
        [self.view displayGreeting];
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //        _greeting = [Greeting new];
        _model = [GreetingModel new];
        _view  = [GreetingView  new];
        _view.controller = self;
        
        _greetingTag = engGreeting;
        _preface = @{ @(engGreeting) : @"Hello",
                      @(rusGreeting) : @"Привет"
                      };
        [self makeGreeting];
        
        NSLog(@"%@ created", self);
    }
    return self;
}

+ (instancetype) greeter {
    return [[self alloc] init];
}

- (void)dealloc
{
    NSLog(@"%@ deleted", self);
}

@end

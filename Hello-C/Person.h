//
//  Person.h
//  Hello-C
//
//  Created by админ on 12.05.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject <NSCoding>

@property (readonly) NSString *firstName, *lastName;

- (NSString *) fullName;

- (instancetype)init;
- (instancetype)initWithFirstName:(NSString *)fName
                      andLastName:(NSString *)lName;
+ (instancetype)personWithFirstName:(NSString *)fName
                        andLastName:(NSString *)lName;

@end

//
//  main.m
//  Hello-C
//
//  Created by админ on 11.04.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GreetingController.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        
        GreetingController *g = [GreetingController new];
        
        [g run];
        
    }
    return 0;
}

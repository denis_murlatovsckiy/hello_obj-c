//
//  Greeting.m
//  Hello-C
//
//  Created by админ on 25.04.16.
//  Copyright © 2016 Student. All rights reserved.
//

#import "Greeting.h"

@implementation Greeting

- (instancetype) initWithString:(NSString *)greetingString {
    self = [super init];
    if (self) {
        _greetingData = greetingString;
        
        NSLog(@"%@ with %@ created", [super description],self.greetingData);
    }
    return self;
}

+ (instancetype) greetingWithString:(NSString *)greetingString {
    return [[self alloc] initWithString:greetingString];
}

- (instancetype)init
{
    return [self initWithString:@"hello"];
}

- (void)dealloc
{
    NSLog(@"%@ with %@ deleted", [super description],self.greetingData);
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", self.greetingData];
}
@end
